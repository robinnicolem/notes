\beamer@endinputifotherversion {3.10pt}
\beamer@sectionintoc {1}{Introduction}{3}{0}{1}
\beamer@sectionintoc {2}{Model description}{5}{0}{2}
\beamer@subsectionintoc {2}{1}{Market microstructure}{6}{0}{2}
\beamer@subsectionintoc {2}{2}{Traders behaviour}{7}{0}{2}
\beamer@subsectionintoc {2}{3}{Simulation results}{9}{0}{2}
\beamer@sectionintoc {3}{Mean field approach}{10}{0}{3}
\beamer@subsectionintoc {3}{1}{Settings}{11}{0}{3}
\beamer@subsectionintoc {3}{2}{Application}{13}{0}{3}
\beamer@subsectionintoc {3}{3}{Consistency with the reinforcement learning dynamics}{16}{0}{3}
\beamer@sectionintoc {4}{Case of an heterogeneous population}{20}{0}{4}
\beamer@subsectionintoc {4}{1}{Motivations}{21}{0}{4}
\beamer@subsectionintoc {4}{2}{The settings}{22}{0}{4}
\beamer@subsectionintoc {4}{3}{Simulation results}{25}{0}{4}
\beamer@sectionintoc {5}{Comclusion}{28}{0}{5}
