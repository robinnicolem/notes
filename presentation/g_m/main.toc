\beamer@endinputifotherversion {3.36pt}
\beamer@sectionintoc {1}{Introduction}{3}{0}{1}
\beamer@sectionintoc {2}{The model}{5}{0}{2}
\beamer@subsectionintoc {2}{1}{Market microstructure}{6}{0}{2}
\beamer@subsectionintoc {2}{2}{Trader behaviour}{7}{0}{2}
\beamer@subsectionintoc {2}{3}{Simulation results}{9}{0}{2}
\beamer@sectionintoc {3}{Finding Nash equilibrium via mean field game theory}{11}{0}{3}
\beamer@subsectionintoc {3}{1}{Mean field game framework}{12}{0}{3}
\beamer@subsectionintoc {3}{2}{Application and comparison with the reinforcement learning dynamic}{14}{0}{3}
