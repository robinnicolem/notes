require(ggplot2)
x <- c(0,0.5,1.)
df <- data.frame('p1'=x,'payoff'= 0.5*x + (1-x))
plot1 <- ggplot(df, aes(x = p1, y  = payoff)) +  geom_line()
+ scale_y_continuous(limits = c(0.5,1), breaks = c(0.5,1), labels = c("payoff1","payoff2")) 
png("payoff_ineq.png")
plot1
dev.off()
