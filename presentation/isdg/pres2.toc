\beamer@endinputifotherversion {3.26pt}
\beamer@sectionintoc {1}{Introduction}{3}{0}{1}
\beamer@sectionintoc {2}{Model description}{6}{0}{2}
\beamer@subsectionintoc {2}{1}{Market microstructure}{7}{0}{2}
\beamer@subsectionintoc {2}{2}{Traders behaviour}{9}{0}{2}
\beamer@subsectionintoc {2}{3}{Simulation results}{11}{0}{2}
\beamer@sectionintoc {3}{Mean field approach}{12}{0}{3}
\beamer@subsectionintoc {3}{1}{Settings}{13}{0}{3}
\beamer@subsectionintoc {3}{2}{Application}{15}{0}{3}
\beamer@subsectionintoc {3}{3}{Consistency with the reinforcement learning dynamics}{22}{0}{3}
\beamer@sectionintoc {4}{Conclusion}{24}{0}{4}
