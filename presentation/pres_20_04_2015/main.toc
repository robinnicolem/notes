\beamer@endinputifotherversion {3.26pt}
\beamer@sectionintoc {1}{Introduction}{3}{0}{1}
\beamer@sectionintoc {2}{Model description}{5}{0}{2}
\beamer@subsectionintoc {2}{1}{Market microstructure}{6}{0}{2}
\beamer@subsectionintoc {2}{2}{Traders behaviour}{7}{0}{2}
\beamer@subsectionintoc {2}{3}{Simulation results}{9}{0}{2}
\beamer@sectionintoc {3}{Mean field approach}{10}{0}{3}
\beamer@subsectionintoc {3}{1}{Settings}{11}{0}{3}
\beamer@subsectionintoc {3}{2}{Application}{13}{0}{3}
\beamer@subsectionintoc {3}{3}{Consistency with the reinforcement learning dynamics}{16}{0}{3}
\beamer@sectionintoc {4}{Large deviation approach for the infinite memory limit}{20}{0}{4}
\beamer@subsectionintoc {4}{1}{Motivations}{21}{0}{4}
\beamer@subsectionintoc {4}{2}{The minimal action path}{27}{0}{4}
\beamer@subsectionintoc {4}{3}{An equation for the market conditions of a segregated population}{29}{0}{4}
\beamer@sectionintoc {5}{Case of an heterogeneous population}{31}{0}{5}
\beamer@subsectionintoc {5}{1}{Motivations}{32}{0}{5}
\beamer@subsectionintoc {5}{2}{The settings}{33}{0}{5}
\beamer@subsectionintoc {5}{3}{Simulation results}{36}{0}{5}
\beamer@sectionintoc {6}{Conclusion}{39}{0}{6}
