r Robin,

It's late here so only quick comments as I'm not sure I'll have time tomorrow:

p4 say "in an ... markets, spontaneous", no full step at end (as bullet points generally aren't full sentences anyway)
Could ... simple -> Can we reproduce segregation in a simple

p6 put the "a" for the footnote after "trading price" in bullet point 2
in the footnote I'd say "<s>=average sell order price, <b>=same for buy"
order which could not -> orders that cannot

p7 period. -> period

p8 Preferences to learn: -> Have to learn preferences among (comes up 2x)
make the |...| signs around "order price -\pi" closer to what they enclose (you can use \!) as hard to read as it stands
otherwise. -> otherwise
Choose -> choose

p9 I'd put "Aloric et al" in the \framesubtitle, and get rid of the footnote

p11 Players -> players
Settings -> Setting (in \subsection title)
I still think the "This is not... limit" isn't that useful here - many people in the audience will not be that familiar with mean field games and won't understand why you're making this point here. I'd leave this off the slide and maybe say it in words.

p13 buy sell -> buy/sell
would be better to make box title
"Scheme of mean field equilibrium criterion"
(avoid "game" for the same reason as on p11)

2 classes -> Two classes of agents

p14
I'd make the title "Equilibrium criterion: two examples"
and leave out the "Example of distributions..." line

Would make the "legend" for the pink box
"Mixed strategies that maximize payoff"

p15 Would give this a title, e.g. "Payoff function". 

the labelling on the graph is still very small. I would instead:
- put the graph in the middle
- on the left, put as a column
"same payoff in
both markets
Supp(phi)\subset[0,1]
segregation is
possible"
and draw an arrow to the horizontal line

- on the right, put as a column
"different payoffs
Supp(phi)=1
segregation is
impossible

- get rid of text and box underneath graph

p16 labels in top left box are very small. How about putting "Payoffs equal?" in large font above the table, then just "c=1" and "c=2" as labels in the top left box
>>> Ro be improved

p17
Get rid of funny space at start of title
say "market and trader settings" to fit title on one line
fonts are small in plot, especially legend

p18 I think this isn't very clear yet. I would emphasize that the equilibrium criterion doesn't determine whether segregation occurs where it's possible, not does it determine the shape of phi1, phi2.
After that you can pose your question as e.g.
"Does the reinforcement learning dynamics lead to Nash equilibria as steady states? Does it resolve the indeterminacy, i.e. select specific equilibria?"

p19 dynamic -> dynamics (3x)

p20 Would just say "Answer: yes!"
I don't know what you mean by "And it is confirmed by analytical results"
Isn't the plot here for the _unsegregated_ solutions of the dynamics? If yes you need to make this clear (and leave off the labels at the top of the graph, or at least say "potentially segregated")

You say "the steady states" here which suggests the steady state at eqch p_b is unique, and then it's hard to understand what is coming on the next slide

p21 you can leave out the "Thanks to..." as it's not a published plot
If you can change the labels then obviously change "Path of this analysis" to something like "Path of analysis so far"

p23 Moyals-> Moyal
Remove footnote and replace by "Aloric et al" as a \framesubtitle
SDE equation -> SDE
preferences single -> preferences of a single
add comma before M_2^{1,2}
last line say instead
"steady state determined from self-consistency: dynamics must give correct C"

p26
Would make title "Determining probability fluxes"
Then bullet points like
"stochastic process in low-noise limit -> can use Freidlin-Wentzell theory"
"need to optimize paths between the two low-noise fixed points of A"
"then tune market conditions to make fluxes equal"
"benchmark: for alpha=1, equality of probability fluxes can be found by different reasoning"
"we compared..."

p27 put commas between the different parameters

p28 not clear as labels are far too small. Also you need to say somewhere that the horizontal line is from the Nash equilibrium!? (Otherwise the message of the plot is lost)

p29 remove

p31 first bullet point ok but settings -> setting
then would say
"Nash equilibria indicate when segregation is possible as function of market parameters"
"But do not predict whether segregation takes place or how"
"Have developed an algorithm to find segregated steady states of dynamics in large memory limit"
"These select specific segregated Nash equilibria: indeterminacy resolved"

p32
dynamic in with -> dynamics with
non stationary -> non-stationary
Comparing -> Compare
move brackets around (Uber... pools)
study ... in the light of -> Explore links to

p33 would leave off - that way you can have the future directions slide up when people ask questions and this prompt more useful questions / discussion

Best wishes,
Peter 
