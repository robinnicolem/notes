(TeX-add-style-hook
 "main"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("beamer" "mathserif")))
   (TeX-run-style-hooks
    "latex2e"
    "beamer"
    "beamer10"
    "tikz"
    "graphicx"
    "booktabs")
   (TeX-add-symbols
    '("fp" 1)
    "T"
    "var"
    "mom"
    "momt"
    "diffus"
    "diffust"
    "prob")
   (LaTeX-add-labels
    "fig:alpha0"
    "eq:fp")))

