%!TEX root = main.tex
\documentclass[main.tex]{subfiles}
%\input{macro.tex}
\begin{document}
\section{Understanding segregation mechanism using mean field game
  theory}
\label{sec:mfg}
A powerful tool to study games in the limit of an infinite number of
players is the \emph{mean field game theory} which has been developed
by Pierre-Louis Lions and Jean-Michel Lasry in \cite{lasry2007mean,
  gueant2011mean}. It is an application of the mean field methods used
in statistical physics to game theory in which the large parameter is
no longer the number of spins but the number of players.
  
The problem I will study is a mean field game in the deterministic
limit using methods of Gu\'eant and al. in \cite{gueant2011mean}. I
will compute the Nash equilibria of the game in the case of perfectly
informed traders who want to maximize their pay-off. I will give a
precise definition of a Nash equilibrium in the limit of infinitely
many players. Then I will apply it to the case of traders with fixed
buy/sell preferences (the case of adaptive buy/sell preferences is
studied in \ref{sec:mfgabs}). Finally I will compare the results
obtained by using the mean field game theory with the one obtained via
reinforcement learning with fictitious play. Then I will discuss the
issue of information disclosure and how it changes Nash equilibria.

\subsection{Finding Nash equilibrium as an optimization problem}
The optimal control problem faced by an agent is: \textit{choose the
  action which will give you the best payoff using the two markets at
  your disposal. Knowing that all the other traders have the same
  goal.}. I consider the case when the environment is deterministic
and agents have reached equilibrium (there is no evolution of the Nash
equilibrium with time). The aim of this section is to present the mean
field game framework in the stationary deterministic case and to apply
it to the model described in section \ref{sec:model}. In the following
description, the population will be composed of two classes of agents
labeled by $c \in \{1,2\}$ which have fixed buy/sell preferences and
buy with probability $\pb{c}$

%% Defining the Pay-off structure

The goal of each trader from a class \(c\)
is to maximize its expected pay-off
$J^{(c)}( \prob,\phib^{(1)},\phib^{(2)})$ defined in the following
equation:
\begin{align}
  \label{eq:mfg1}
  J^{(c)}(\prob,\phib^{(1)},\phib^{(2)}
  ) = \sum_{\gamma}\proba{\gamma}
  \mbox{Payoff}^{(c)}(\gamma,\phib^{(1)},\phib^{(2)} )
\end{align}
where:
\begin{itemize}
\item the mixed strategy he chooses $\prob =\{\proba{\gamma}\}_\gamma$,
  \item the average payoff of action $\gamma$: $\mbox{Payoff}^{(c)}(\gamma,\phib^{(1)},\phib^{(2)} )$ which is a function of the distribution of the mixed strategies of the different
classes of agents: \mbox{$\phib^{(c)} : [0,1] \rightarrow \mathbb{R}_+$} (where $[0,1]$ is the set of mixed strategies available to class $c$. Some example of mixed strategies distributions are given in table \ref{tab:msdist})
\end{itemize}
%% Definition of a Nash equilibrium
\begin{table}[top]
  \centering
  \begin{tabular}{|c|c|}
    \hline 
    Population behaviour & corresponding distribution $\phib(x)$ \\
    \hline
    All the agents always go to market 1 with probability $p$ & $\delta(x-p)$\\
    Half of the agents always to market 1, the rest always go to market 2 & $\frac{1}{2}(\delta(x) + \delta(1-x))$ \\
    \hline
  \end{tabular}
  \caption{Example of probability distribution of mixed strategy for agents with fixed buy sell preferences}
  \label{tab:msdist}
\end{table}
Using the pay-off defined in equation \eqref{eq:mfg1}, it is possible
to give a definition of a Nash equilibrium in the limit of an infinite
number of players. In this case, it is no longer characterized by a
set of strategies but by a distribution over mixed strategies labelled
$\phib^{(c)}$. A necessary and sufficient condition for $\phib^{(c)}$
to be a distribution associated to a Nash equilibrium is that all the
strategies which can be done within the distribution $\phib^{(c)}$ of
actions should maximize the total pay-off of each trader given the
behaviour of class $c$ induced by $\phib^{(c)}$. The mathematical
equivalent of this condition is
\begin{align}
  \label{eq:mfg2}
  &\forall c \in \{1,2\} \;\;
    \mathrm{Support}(\boldsymbol{\phii})\in
    \argmax_{\prob}\left[J^{(c)}(\prob,\phib^{(1)},\phib^{(2)}
    )\right]
\end{align}
Hence the problem of finding the Nash equilibrium of the traders is
turned into two coupled optimization problems (one for each
class). Using this formulation we will study the Nash equilibrium in
the cases of \emph{fixed buy/sell preferences}

% Now that we have defined the general framework to study the
% stationary probability distribution of the traders. We will see two
% specific cases. First, the case of two populations of traders with
% fixed buy/sell preferences which have different willing to buy, then
% the case of traders with adaptive buy/sell preferences.
  
\subsection{Case of traders with fixed buy/sell preferences}
In the case of traders with fixed buy/sell preferences, the pure
strategies available to the traders are: send an order to market 1 and
send an order to market 2, ($\gamma \in \left\{m=1,m=2\right\}$) and,
the type of the order is a bid with probability $\pbb$. The
corresponding set of mixed strategies will be parametrized by a single
number $p_1$: the probability to buy at market 1. In that case, the
payoff of equation \eqref{eq:mfg1} has the following form
\begin{align}
  \payofffbfsmi{p_1} = p_1 \payofffbfspu{1}{c} + (1-p_1) \payofffbfspu{2}{c}
\end{align}
\begin{align}
  \label{eq:mfg3}
  & \text{Payoff}^{(c)}(m,\phib^{(1)},\phib^{(2)} )&=&\pb{c}
                                                       \Tm{b}
                                                       \min \left(1,
                                                       \frac{(1-\pb{1})\fm{m}{1}+(1-\pb{2})\fm{m}{2}}
                                                       {\pb{1} \fm{m}{1} + \pb{2}\fm{m}{2}}
                                                       \frac{\Tm{s}}{\Tm{b}}
                                                       \right)
                                                       \frac{\Sm{b}}{\Tm{b}} + \notag\\
  &&& (1-\pb{c}) \Tm{s}
      \min \left(1,
      \frac{\pb{1}
      \fm{m}{1} + \pb{2}\fm{m}{2}}
      {(1-\pb{1})\fm{m}{1}+(1-\pb{2})\fm{m}{2}}
      \frac{\Tm{b}}{\Tm{s}}\right)
      \frac{\Sm{s}}{\Tm{s}}
\end{align}
Where
\begin{itemize}
\item $T_\gamma$ with $\gamma \in \left\{A1,B1,A2,B2 \right\}$ is the
  chances one has to submit a valid order when playing action
  $\gamma$.
\item $\bar{S}_\gamma$ is the average score you get once you know that
  you sent a valid order which was matched if you have played action
  $\gamma$.
\item $\fm{m}{c}$ is the fraction of class $(c)$ at market $m$ which
  is inferred from the probability density function of group $g$, this
  is the mean of $p^{(c)}_1$
\item $\pb{c}$ is the probability to buy of a trader from class $c$
\end{itemize}
The formula of the payoff below is the sum of two terms: the first one
related to the payoff when buying weighted by the probability of
buying and the second one which is the payoff when selling weighted by
the probability of selling. The term $T_\gamma$ is the probability
that you send an order which is valid regarding the trading price. The
term $\min(1,\cdots)$ is the chances that a valid order you send is
successfully matched with an order on the other side of the
market. The last term of the form $\bar{S}_\gamma/T_\gamma$ is the
average score you will earn if you sent an order which was executed.
%%\todo{add an appendix with the value of those quantities}
%% Characterization of the corresponding optimization problem

For fixed distribution of strategies of the populations which in our
case implies fixed $\fm{m}{c}$, the payoff of a single agent is an
affine function of the probability to buy at market 1: $\pmm$. Hence,
the problem \eqref{eq:mfg2} is to maximize an affine function over a
probability interval.

% segregated equilibria case
When the pay-off function is not constant with respect to $\pmm$, as
illustrated in figure \ref{fig:linmin}, the values of $\pmm$ which
will maximize the payoff will be on the boundary of the probability
interval. This means that the strategies which will maximize a trader
payoff cannot be mixed.  In this case all the traders from the same
class will be trading at the same market, hence they cannot
segregate. This kind of equilibrium is then called \emph{unsegregated
  Nash equilibrium}.

% unsegregated equilibria case
In some other cases, when the pay-off function is constant with
respect to $\pmm$, i.e. for fixed quantities $\fm{m}{c}$, the average
payoff when trading at market 1 is the same as when trading at market
2. Hence, any value of $\pmm$ will be a solution to the maximization
problem. This implies that the traders can possibly segregate. This
type of equilibria is then called \emph{ segregated Nash equilibrium}.

\begin{figure}[h!]
  \label{}
  \centering
  \begin{tikzpicture}[xscale =1.8]
    \draw[<-] (0,3.5) -- (0,-1) ; \draw[->] (0,-1) -- (3.5,-1) ;
    \draw[red] (0,0) -- (3,3) ; \draw[->] (0.5,0.25) -- (1.,0.75) ;
    \draw[->] (0.25 + 1.75 ,0.5 + 1.75 ) -- (0.75 + 1.75 ,1 + 1.75 ) ;
    \draw[->] (0.5 + 1.75 ,0.25 + 1.75 ) -- (1. + 1.75,0.75 + 1.75) ;
    \draw (1.5,-1) node[below] {$p$}; \draw (0,3) node[left]
    {$\Pi_1$}; \draw (0,0) node[left] {$\Pi_2$}; \draw (3,-1)
    node[below] {1}; \draw (0,-1) node[below] {0}; \draw[red] (3,3)
    node[right] {different payoff (case (1))}; \draw[dashed] (0,3) --
    (3,3) ; \draw[dashed] (3,-1) -- (3,3) ; \draw[->] (0.25,0.5) --
    (0.75,1) ; \draw[blue] (0,1.5) -- (3,1.5) ; \draw[blue] (3,1.5)
    node[right] {equal payoff (case (2))};
  \end{tikzpicture}
  \caption{Minimization of the payoff in the two different cases. The arrows show the sense in which you would change your probability to go to market 1 $p$ in order to maximize your payoff}
  \label{fig:linmin}
\end{figure}

\begin{figure}[h!]
  \centering
  %% \includegraphics[scale=.5]{diagmfg.png}
  % \includegraphics[scale=0.3]{diagmfg.png}
  \begin{tikzpicture}[scale = 1., transform shape]
    %% drawing the node
    \node[draw] (O) at (0,0) [fill=blue!25]{Number of groups with
      equal payoff}; \node[draw] (2) at (-1.5,-1) [fill=red!75]{2};
    \node[draw] (1) at (0,-1) [fill=green!75]{1}; \node[draw] (0) at
    (1.5,-1) [fill=blue!75]{0}; \node[draw] (seg) at (-4.5,-3.2)
    [fill=blue!25,text width=2cm]{Possibly segregated equilibrium };
    \node[draw] (partseg) at (-1.5,-3.2)[fill=blue!25,text
    width=2cm]{Possibly partially segregated equlibrium}; \node[draw]
    (unseg) at (3,-3) [fill=blue!25,text width=2.1cm]{Unsegregated
      equilibrium}; \node[draw] (ind) at (3,-5) [fill=orange!80,text
    width=5cm]{Both sub-populations trade at the same market};
    \node[draw] (unsegas) at (1.5,-7.5) [fill=blue!25,text
    width=2cm]{Unsegregated anti-synchro eq.}; \node[draw] (unsegs) at
    (5,-7.5) [fill=blue!25,text width=2cm]{Unsegregated synchro eq. };
    \draw (5.7,-6)node[left]{\textbf{YES}} ; \draw
    (1.5,-6)node[left]{\textbf{NO}} ;
    %% drawing edges
    %% first level of nodes
    \draw[->,>=latex] (O) to[bend left] (2); \draw[->,>=latex] (O) to
    (1); \draw[->,>=latex] (O) to[bend right] (0);
    \draw[->,>=latex,color=red] (2) to[] (unseg);
    \draw[->,>=latex,color=red] (2) to (partseg);
    \draw[->,>=latex,color=red] (2) to[bend right] (seg);
    \draw[->,>=latex,color=green] (1) to[] (unseg);
    \draw[->,>=latex,color=green] (1) to (partseg);
    \draw[->,>=latex,color=blue] (0) to[bend left] (unseg);
    \draw[->,>=latex,color=black] (unseg) to (ind);
    %% second level of nodes
    \draw[->,>=latex] (ind) to[bend right] (unsegas);
    \draw[->,>=latex] (ind) to[bend left] (unsegs) ;
  \end{tikzpicture}
  \caption{Diagram of the possible Nash equilibria and their condition
    of existence}
  \label{fig:mfg1}
\end{figure}

% Conclusion
Using the different cases we saw previously, it is possible to
classify the Nash equilibria of the whole population as shown in
figure \ref{fig:mfg1}. The main criteria are: how many of the two
classes are segregated and if the classes are unsegregated, are the two
classes trading at the same market (synchronized) or are they trading
at a different market (anti-synchronized),

\subsection{Numerical results}

% Segregation in symmetric settings
In figure \ref{fig:mfg2}, the zones of existence of possibly
segregated equilibria and anti-synchronized equilibria and partially
segregated equilibria are plotted. From the numerical evaluation in
the case where $\pb{1} = 1- \pb{2}$ and $\theta_1 = 1 - \theta_2$
which we call symmetric settings, there seems to be two parameters
which inhibit the segregation of traders. The first one is the
similarity between the two markets, the more different price setting
parameters of the markets are, the more likely it is that traders will
not segregate. The second one is the similarity between the two
populations of traders, the more the probability to buy of both 
populations will be close to each other, the more likely it is that the
population won't segregate.

% Influence of asymetry
The phase diagram presented in the figure \ref{fig:mfg2} is obtained
with symmetric price setting parameters and probability of buying of
both populations also symmetric. Will the results be quantitatively
different if this symmetry is broken? From the numerical analysis, one
sees that the zone of non-possibility of segregation vanishes when the
symmetry of the system is broken.

\begin{figure}[h!]
  \centering
  \includegraphics[scale=.7]{images/mfg/nasheq.png}
  \caption{plot of the zone of stability of the unsegregated
    anti-synchronized Nash equilibrium (pink). The numerical
    analysis show that in the case where the parameters of the
    market and the preferences to buy of the populations are
    symmetric, the anti-synchronized and segregated Nash equilibrium are mutually exclusive.%   $(p_b,\theta)$ will be
    % compatible with an unsegregated Nash equilibrium if and only if it
    % is not compatible with a segregated one
  }
  \label{fig:mfg2}
\end{figure}
% \begin{figure}[h!]
%   \centering
%   \includegraphics[scale=0.7]{phidiag.png}
%   \caption{Diagramm of the zone of existence of different types of
%   Nash equilibria when $th = \theta_1 = 1 - \theta_2$ and
%   $p_b^{(1)} = 1- p_b^{(2)}$. As it is said in the previous remark,
%   in the zone where there is no anti-synchronized unsegregated
%   equilibrium there is existence of a fully segregated Nash
%   equilibria}
%   \label{fig:mfg3}
% \end{figure}
% \begin{todo}
%   Include discussion of the results of the phase diagram
  
%   There does not exist a segregated equilibrium in symmetric
%   settings when~:
%   \begin{itemize}
%   \item markets have $\theta$ different enough and
%   \item the population characteristics $p_b$are cloth enough
%   \end{itemize}
% \end{todo}
\subsection{The information availability in the mean field model}

Unlike the previous model based on reinforcement learning described in
section \ref{sec:model}, in this one, traders are fully informed about
the history of trades in both markets. One can wonder how this
difference between the two models will affect segregation. My
intuition which I have not yet verified by numerical simulations is
that the segregation is less likely to occur in the mean-field setting
than in the general model where traders update their preferences via
reinforcement learning. As it has been pointed out in the studies of
\cite{madhavan1995consolidation} segregation occurs when some traders
have a lack of information about the state of the market, but this
might not be true in our model. For example, in our model, when
traders choose their actions using reinforcement learning, they might
trade with a market which is the least profitable to them because they
are afraid of the loss they will face by exploring the other
possibility to trade. This exploration exploitation trade-off which is
an important issue in reinforcement learning problems does not appear in
the mean field model.
\subsection{Comparison with the reinforcement learning dynamics in the
  limit of fictitious plays}
\label{sec:mfgcmp}

Now that we have a method which gives useful information about the
behaviour of traders in the full information settings, we want to
compare it with the reinforcement learning dynamics developed in
section \ref{sec:model}. Intuitively, in the case of score updating
using fictitious play ($\alpha = 0$) and in the limit of non
forgetting traders ($r \rightarrow 0$), the preference of a trader
toward any action is exactly the average of the score he actually
obtained by playing this action. Hence, it would be likely for the
steady state of the corresponding dynamic to be the same as the Nash
equilibrium of the game in the full information settings with
$T \rightarrow 0$.  In the rest of this section, I will compare the
Nash equilibrium of the game previously defined with the dynamic
described in section \ref{sec:model} in the limit $r \rightarrow 0$,
$T \rightarrow 0$ and $\alpha \rightarrow 0$ using a continuous equation from \cite{saskareport}, which is also the Fokker Planck equation I will talk about in section \ref{sec:varpop} with $r = 0$ . I will show that the
deterministic equation of the reinforcement learning dynamic in the
limit $r \rightarrow 0$ has for fixed point any Nash equilibrium
obtained via the mean field approach. I will also prove the stability of
those states under a single agent dynamic (considering a fixed population
behaviour). The figure \ref{fig:fpvsmfg} shows the convergence of
the steady states of the deterministic dynamic to the Nash equilibria
as $T$ goes to $0$.
\begin{figure}[h!]
  \centering
  \includegraphics{images/mfg/nash_eq_fbfs.png}
  \caption{Fraction of traders going to market 1 as a function of
    $p_b$ with $\theta = 0.3$ and symmetric settings. The dashed lines
    represent the results of the Fokker Planck equation for different
    Temperature ($ T = 0.01 $ green, $T=0.001$ pink, $T = 0.0001$ red)
    the continuous black lines are the Nash equilibrium found via Mean
    field. There is some points of the deterministic reinforcement
    dynamic when $\pbb$ is close to $0.5$ which have for y coordinate
    $1$ or $0$. They are the Nash equilibria which are
    anti-synchronized.}
  \label{fig:fpvsmfg}
\end{figure}

\subsubsection{Analytic results}
% theoretical results
Let us define
$\{\pref{m}{g}\} =
\left\{\pref{1}{1},\pref{2}{1},\pref{1}{2},\pref{2}{2}\right\}$
and define $\phi$ to be the corresponding probability
distribution. The deterministic Fokker Planck equation in the case of
preferences updating with fictitious ($\alpha = 0$) play is:
\begin{align}
  \label{eq:fp}
  \partial_t\pref{1}{1} =&  \left(J_1^{(1)}(\phi) - \pref{1}{1} \right) \probme{1}{1} \\
  \partial_t\pref{2}{1} =&  \left( J_2^{(1)}(\phi) - \pref{2}{1} \right)\probme{2}{1} \\
  \partial_t\pref{1}{2} =&  \left( J_1^{(2)}(\phi) - \pref{1}{2} \right) \probme{1}{2} \\
  \partial_t\pref{2}{2}=& \left( J_2^{(2)}(\phi) - \pref{2}{2}
                          \right) \probme{2}{2}
\end{align}
where $J^{(c)}_m$ is the payoff of a trader from class $c$ if he
chooses market $m$, and $\mathrm{P}^{(c)}(m=j)$ is the probability that
the agent from class $c$ goes to market $j$ given his current
preferences\footnote{it is infer from the soft-max rule in the
  reinforcement learning algorithm}.

In order to find the fixed point of the traders dynamic and assess its
stability, we will use the following process. First we will compute
the fixed point of the reinforcement learning dynamic at the
population level. Then we will check if this state is stable with
respect to the single agent dynamic when the quantities $\phi$ are
kept fixed (their value is the one they had in the steady state of the
population level dynamics )

%% the scores reflect the payoff
First, we see that in the limit $T \rightarrow 0$, the scores of the
agents in the steady states is exactly the average payoff of the
corresponding action. This is due to the following equation which is verified
in the steady state of the reinforcement learning dynamic:
\begin{align}
  \label{eqproof}
  \forall i \ \forall m \ \left( J_{m}^{(i)}(\phi) - \mathbf{A}^{(i)}_{m} \right)
  \mathrm{P}^{(i)}(m=m) = 0
\end{align}

In the limit $T \rightarrow 0$, if the strategy corresponding to the
steady state is mixed, then the expected payoff of all the available
actions is equal. This is due to the fact that, in the limit where the
temperature of the agents goes to zero, one expects the population not
to act using pure strategies i.e. the whole population goes to market
1 or the whole population goes to market 2. This means that the limit
when T tends to zero of the sigmoid $\sigma_T(\Delta A)$
function must not be $0$ or $1$.
\begin{align}
  \label{eq:sigmoid}
  \sigma_T(\Delta A) = \frac{1}{1+\exp{\frac{\Delta A}{T}}}
\end{align}
This condition is achieved only if $\Delta A \sim T$ this implies that
in the limit of small $T$, the expected payoff when a mixed strategy
is played, the corresponding preferences are equal. From the previous
paragraph, this implies that the expected payoff of the actions are
equal.

% \begin{definition}[suitable fixed points]
%   let suppose that $\mathbf{x}_T$ is a sequence of fixed points of
%   the Fokker Planck dynamic. If
%   \begin{itemize}
%   \item $\mid x_1 - x_2 \mid \sim_{T\rightarrow 0 } T$ and
%     $\mid x_3 - x_4 \mid \sim_{T\rightarrow 0 } T$
%   \item the corresponding probability to buy to market 1 of both of
%     the populations doesn't converge to 0 or 1.
%   \end{itemize}
%   then the sequence of fixed points is said to be a suitable
%   sequence of fixed points
% \end{definition}

Now that we computed the steady state of the deterministic dynamic,
let us study its stability at the agent level. The stability matrix of
the agent level dynamic in equation is shown below and has only
strictly negative eigenvalues which implies that the steady states are
stable.
\begin{equation}
  S = \left(
    \begin{array}{cccc}
      -\probme{1}{1} & 0 & 0 & 0  \\
      0 &-\probme{2}{1} & 0 & 0  \\
      0& 0 &  -\probme{1}{1} & 0   \\
      0 & 0  & 0 & - \probme{2}{1}   \\
      
    \end{array}
  \right)
\end{equation}

The reasoning I described in this section demonstrates two important
properties of the steady state in a particular case of the reinforcement
learning dynamic we studied:
\begin{itemize}
\item it is the Nash equilibrium of the mean field game we defined in this section 
\item it is \emph{stable with
    respect to the single agent dynamic}
\end{itemize}


\subsection{Discussion}
In this section I computed all the Nash equilibria of the game played
by the traders when they are fully informed about the market
conditions. In this case, the only case in which there will exist
segregated equilibria is when the payoff of both markets will
be the same. Even though this reasoning doesn't give cases in which
segregation will occur for sure, it gives cases in which segregation
won't exist. In addition, as it is explained in the section
\ref{sec:mfgcmp}, the forecast of the mean field theory agrees with the
steady state of the reinforcement learning dynamic in the proper
limit, unfortunately the corresponding solution is not segregated. The
question one asks now is: \emph{Can we take a limit $(\alpha,T)$ for
  which the corresponding solution of the Fokker Planck equation will
  remain segregated ?}

\end{document}
%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "main"
%%% End: 



