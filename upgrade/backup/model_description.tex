\documentclass[main.tex]{subfiles}
% \input{macro.tex}
\begin{document}
\section{Description of the model}
\label{sec:model}
The goal of this section is to present a minimal double auction market
model built by Aloric et al. \cite{saska} which is based on
reinforcement learning. As shown in their numerical simulations, even
in those simple settings, segregation of the traders is observed.

%% Short description of the model
The considered economical system is composed of $2$ markets and $N$
traders. A complete trading session is subdivided into many trading
steps at the beginning of which, all the traders are forced to trade
one unit of good at only one market. Once all the orders have been
sent, each market clears the orders using the following discrete
double auction mechanism:

\begin{enumerate}
\item a trading price \(\pi_m\)
  is set according to the mean of the received bids and asks using
  $\theta_m$ a parameter specific to the market as it is shown in the
  following equation
  \begin{align}
    \label{eq:mod1}
    \pi_m = \theta_m \langle b \rangle + (1-\theta_m) \langle a
    \rangle
  \end{align}
\item All the orders which are not compatible with the trading price
  (i.e. bids that are lower than $\pi_m$ or asks that are higher than
  $\pi_m$) are rejected. The remaining orders called \emph{valid
    orders}
\item Finally all the remaining orders called \emph{valid orders} are
  matched pairwise and the trading occurs at the trading price. The
  orders which could not be executed are rejected.
\end{enumerate}

%% Description of zero intelligence traders
The traders considered here are zero intelligence: the prices for
their orders to buy (resp. sell) are Gaussian random variables with
mean \(\mu_b\)
(resp. \(\mu_a\))\footnote{The
  typical values for $\mua$ and $\mub$ are 11 and 10} and variance
\(1\).
This leaves the choice of the actions (at which market to trade and
the type of order to send) to be chosen.  This is done via
reinforcement learning.

%% Presentation of the reinforcement learning algo
We consider two different scenarios which will be characterized by the
way reinforcement learning will be performed by the traders. The first
one is: traders place orders to buy at fixed rate
$\pbb \in \left[ 0,1\right]$. Then the action to be learned by
reinforcement learning is the market to which the order is to be
sent. This type of trader will be labeled \emph{fixed buy/sell
  preferences}. In the applications, the population will always be
constituted of two classes of traders with different probability to buy $\pbb$ It is also possible to consider traders who do not have
constraints on the rate at which they are going to send bids, hence
they will learn which market to send the orders to and also the type
of the order (bid or ask) to be sent. We will call this type of
traders \emph{adaptive buy/sell preferences}. Considering the set of
possible actions \(\gamma \in \Gamma\)
which depends on the scenario we consider which are summarized in table
\ref{tab:tradtype} and the
corresponding preferences \(A_\gamma\),
the reinforcement learning process consists in updating your
preferences after each clearing process according to the following
equation:
\begin{align}
  \label{eq:mod3}
  A_\gamma(t+1)=\left\{ \begin{array}{ll}
                          (1-r) A_\gamma (t)  + r S_\gamma (t) &  \text{ If action \(\gamma\) was chosen}  \\
                          (1-\alpha r) A_\gamma (t) & \text{ else}
                        \end{array}\right.
\end{align}
\begin{align}
  \label{eq:mod4}
  S_\gamma = \left\{\begin{array}{ll}
                      0 & \rm{if\ the\ order\ was\ rejected} \\
                      \rm{bid} - \text{trading price}& \rm{if\ bid\ was\ accepted} \\
                      \text{trading price} - \rm{ask} & \rm{if\ ask\ was\ accepted}
                    \end{array}\right.
\end{align}

Where $r$ is a forgetting parameter and $\alpha$ controls how a trader
updates the scores of the actions he did not play by using the following
reasoning. At each step the \emph{fictitious score} you would have
earned if you had played action $\gamma$ is inferred to be
$\hat{S}_\gamma = (1-\alpha) A_\gamma$ where $A$ is the value of preference toward action $\gamma$
Then the preferences toward all the actions are
updated using the following rule:
\begin{equation}
  A(t+1) = (1 - r)A(t) + r\hat{S}
\end{equation} The lower $\alpha$ will be, the more the
previous actions will be forgotten. For example, if an action is not
played between time step $t_0$ and time step $t_0 + m$, then
$A_\gamma$ will have decreased by a factor $(1-\alpha r)^m$. In the
case $\alpha = 0$ the preference toward the actions which were not
played is not decreased and hence the forgetting which occurs in the
case $\alpha \neq 0$ does not take place. The update of the scores is then said to be done using \emph{fictitious play}

Once the preferences toward each actions have been updated, the action
to perform in the next trading step is randomly chosen using the
soft-max rule:
\begin{align}
  & \mathrm{P}_\gamma \propto
    \exp(A_\gamma/T)
    \label{eq:mod2}
\end{align}


\begin{table}[t]
  \centering
  \begin{tabular}{|cc|}
    \hline
    Type of trader & available actions  \\
    \hline
    Fixed Buy/Sell preferences & $\{m=1,m=2\} $ \\
    Adaptive buy/sell preferences & $\{\rm{buy@1}, \rm{buy@2}, \rm{sell@1}, \rm{sell@2}\}$ \\
    \hline
  \end{tabular}
  \caption{Actions available depending on the types of traders}
  \label{tab:tradtype}
\end{table}

\subsection{Results of the simulation}
The results of the simulations which are plotted in figure
\ref{fig:simad} show that even with a moderate number of agents, when
the temperature is small enough, agents specialize in one of the
four available actions. On the contrary, when the temperature is high
enough, the traders do not specialize and most of the population have
equal preferences toward the four possible actions.
\begin{figure}[t]
  \centering
  \begin{tabular}[h!]{cc}
    \includegraphics[scale=0.45]{images/intro/T015.jpg} &
                                                          \includegraphics[scale=0.45]{images/intro/T02.jpg} \\
    (a) T = 0.14 & (b) T = 0.29
  \end{tabular}
  \caption{From \cite{saska}. In the case of traders with adaptive
    buy/sell preferences, distribution of the traders preferences with
    $N = 200$ agents, 2 markets, $\theta_1 = 1 - \theta_2 = 0.3$,
    $\mub - \mua = 1 $, $r = 0.1$ }
  \label{fig:simad}
\end{figure}
% A good quantity to assess the segregation is the B
%inder cumulant
% $B_c = 1 - \frac{\langle x^4\rangle}{3 \langle x^2 \rangle^2}$ which
% will be equal to 0 for a Gaussian distribution but will be different
% from zero for bi-modal distribution.
An interpretation of the segregation phase would be that the traders
heterogeneously learn to specialize in a different mixed
strategy. Some of them finally play a strategy which provides them with
a regular payoff while some others have a strategy which provides them
with a less regular but higher pay-off. Those categories of traders
are summarized below:
\begin{itemize}
\item The \emph{profit oriented} traders are going to trade at a
  market where the trading price will make their profit higher in
  average, but, their orders will be rejected more often.
\item The \emph{volume oriented} traders are going to trade at a
  market where, their profit will be lower in average, but, their
  orders will be rejected less often
\end{itemize}

One can also compare the strategy used in the light of Markowitz
portfolio optimization theory \cite{markowitz1952portfolio} where the
volume oriented traders would have a low variance and low profit
strategy and the profit oriented ones have a high variance and high
profit strategy.

%% Comparison with the portfolio optimization strategy

\end{document}
%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "main"
%%% End: 
