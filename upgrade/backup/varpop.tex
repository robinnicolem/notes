\documentclass[main.tex]{subfiles}
%\input{macro.tex}
\begin{document}
\section{Influence of the information heterogeneity}

\label{sec:varpop}

%% INTRO bibliography + problematic
In model defined in section \ref{sec:model}, the population of traders
was homogeneous. This is not the case in real economical systems where
everybody trades for different reasons and does not value the traded goods
with the same utility function. For example, we can distinguish four
categories of traders
\begin{itemize}
\item The speculators who trade for immediate profit
\item The investors who trade for profit in the long run
\item The arbitrageur who will take advantage of the market
  inefficiency to make money
\item The hedger who will trade to manage the risk in their portfolio
  of actions
\end{itemize}
In order to have a more realistic model, we would like to add
heterogeneity to the population which would be composed of both
speculators and investors. The parameter associated to the reaction
timescale would be the forgetting parameter. The lower it will be the
closer to speculator the behaviour of a trader will be.

%% EXTENDING THE MODEL : Using forgetting parameter to model how
%% traders are informed and labeling fast vs slow traders according to
%% fparam
In our model for example, if we set the forgetting parameter of a
fraction of the population to $1$, the learning dynamic of the traders
should differ from the one of an homogeneous population and be closer
to the one of population which is composed of both speculators and
investors. The difference by, at least one order of magnitude between
forgetting parameters of those two types of traders, implies that
their characteristic relaxation time to equilibrium will also differ
by one order of magnitude.  ($\tau_{r \neq 1} \gg \tau_{r = 1}$). This
justifies the labeling of \emph{fast} for the agents with forgetting
parameter $r = 1$ and of \emph{slow} for the one with $ r \neq 1$ as
it is show on table \ref{tab:label}.
\begin{table}[t]
  \centering
  \begin{tabular}{|c|c|c|}
    \hline
    Traders label & forgetting parameter & Memory length \\
    \hline
    Fast traders & $r = 1$ & Short memory \\
    Slow traders & $ r \neq 1$ & Long memory\\
    \hline
  \end{tabular}
  \label{tab:label}
  \caption{Label of the traders depending on their forgetting parameter}
\end{table}


%% METHODS used to study a population of heterogeneous traders fp
%% markovian equation and fp fokker planck equation


The study of the steady state of a population with heterogeneous
memory traders will be done using two methods: a multi-agent
simulation and an analytic solution of the Fokker-Plank equation for
the slow agents coupled with a fixed point equation for the fast
agents.

\subsection{Study of an heterogeneous population}

%% INTRO

I will present in this section the algorithm I used to compute the
distribution of an heterogeneous population of agents in the fixed
buy/sell preferences setting.

%% SASKA ALGORITHM presenting the fixed point approach ised by saska
%% to solve the Fokker Planck equation by iterating the market
%% conditions up to the correct

In \cite{saska} Aloric et al. use a Kramers Moyal's expansion to
derive a Fokker Planck for the distribution of the scores in an
homogeneous population of slow agents updating their choices via
reinforcement learning. In the limit of an infinite number of traders,
average of the received bids and ask are fixed due to the law of large
numbers, this implies that the trading prices which are linear
combinations of those two quantities will also be fixed. Starting from
the master equation, a Kramer Moyal's expansion in the limit of small
forgetting parameter was done which lead to a Fokker Planck equation
with first and second order moments $M_1(\Delta A,P)$ and
$M_2 (\Delta A, P)$ which are computed in the
appendix\ref{fixedpointfpappendix}. This Fokker Planck equation is not
linear because $M_1$ and $M_2$ are functionals of the probability
density function $P$.
\begin{align}
  \label{eq:fp}
  \partial_t
  P_t(\Delta A) = -\frac{\partial}{\partial \Delta A}\left( \mathrm{M}_1(\Delta A,P_t)
  P_t(\Delta A)\right) + r \frac{\partial^2 }{\partial \Delta A^2}
  \left( \mathrm{M}_2(\Delta A,P_t)P_t(\Delta A)\right) 
\end{align}

%% Explanation of the fixed point method

% The stationary form of this equation using is then solved using a
% fixed point approach. Noticing that the first and second order
% moment in the Fokker Planck equation depend on the distribution
The stationary form of this equation is then solved using a fixed
point approach on the market conditions $\rm{T}_m$ defined below.

\begin{equation}
  T_m = \frac{\text{\# valid bids @ market m}}{\text{\# valid asks @ market m}}
\end{equation}

When the market conditions are fixed, the stationary Fokker Planck
equation to be solved becomes a linear equation. Hence, it is possible
to solve it in closed form. Taking advantage of this fact, Aloric et
al. build a sequence of solutions which converges to the actual
solution of the stationary Fokker Planck equation \eqref{eq:fp}.

Starting from a guess of the market conditions,
$\mcond{1} = \{\mcond{1}_1,\mcond{1}_2\}$ they compute the analytic
solution of the corresponding linear Fokker Planck equation for fixed
market conditions which is called $P(.)^{(0)}$. Then, they compute the
new market conditions corresponding to the probability density
$P(.)^{(0)}$ : $\mcond{1}$. Using the new market conditions instead of
the first guess, they compute a new solution of the linear Fokker
Planck equation with fixed market conditions $\mcond{1}$ which will be
called $P(.)^{(2)}$. Iterating this process until the market
conditions $\mcond{n+1}$ and $\mcond{n}$ are close enough should give
a good numerical approximation of the stationary solution of
\eqref{eq:fp} The distribution obtained via this algorithm in the case
of an homogeneous population was highly consistent with the results
found via multi-agent simulations. This justifies a-posteriori the
approximations (fixed trading price, fixed market conditions,
convergence of the fixed point iterations...). I adapted this
algorithm to the case of an heterogeneous population and the results
as it is shown in figure \ref{fig:fpvarpop} are also consistent with
multi-agent simulations.

%% EXTENDING SASKA ALGO: how did I extend aleksandra algorithm to the
%% problem I am studying

In the case I am studying, there are two sub-populations with
different forgetting parameters which are interacting with each other
through the markets.  Using the difference between the timescale of
their dynamic, it seems reasonable to suppose that the fast agents are
always in equilibrium with the slow ones (in the limit $r \rightarrow 0$
this is an exact approximation). This justifies the algorithm
described in above to compute the steady state of the slow traders in
an heterogeneously informed population by using the fixed point
equation derived in \ref{ap:varpop} to compute the steady state of
fast agents.

Let us label as in the previous description $\mcond{i}$ the market
conditions at time $t$. There are now two groups of agents : the fast
and the slow. At each time-step $t$
\begin{enumerate}
\item \label{l1} the fast agents equilibrate with the slow agents
  using the market conditions at time $t-1$ hence the market
  conditions are modified and become $\tilde{T}^{(t)}$
\item using the market conditions $\tilde{T}^{(t)}$ the slow agents
  update their probability distribution using the fixed point method
  described previously.
\item iterate until the market conditions converge
\end{enumerate}
% \begin{figure}[h!]
%   %   \todo{improve the algorithm}
% \begin{verbatim}
% function fastagents({nslow},frac) 
% 	return {nfast}
% function onestepslowagents({nslow},{nfast},frac) 
% 	return {nslow}

% while stopping_condition do
%     TF <- fastagents({TS},frac)
%     TT <- f(TF+TS)
%     TS <- onestepslowagents(TT,frac)
% end do
% return ({TF},{TS})
% \end{verbatim}

%   \centering
%   %%   includegraphics{diagvarpop.png}
%   \begin{tikzpicture}[scale = 1.]
%     \node[draw, rectangle,fill=blue!25,text width=3cm] (sag) at
%     (0,1) {Slow agents fixed point iteration} ; \node[draw,
%     circle,fill=red!25] (Ts) at (3,2) {$T_{slow}$} ; \node[draw,
%     circle,fill=red!25] (Tt) at (3,0) {$T_{total}$} ; \node[draw,
%     circle,fill=red!25] (Tf) at (5,0) {$T_{fast}$} ; \node[draw,
%     rectangle,text width=3cm,fill=blue!25] (fastag) at (8,1) {Fast
%     agents Fixed Point iterations} ;
%     %%     Draw edges
%     \path[->] (Ts) edge (Tt); \path[->] (Ts) edge (fastag);
%     \path[->] (Tt) edge (sag); \path[->] (fastag) edge (Tf);
%     \path[->] (sag) edge (Ts); \path[->] (Tf) edge (Tt);
%   \end{tikzpicture}
%   \caption{Algorithm in pseudo language and the corresponding
%   scheme}
%   \label{algo}
% \end{figure}

% Using the distribution generated by the algorithm in figure
% \ref{algo} I computed the Binder cumulantc
% $B_c = 1 - \frac{m_4}{3 m_2^2}$\footnote{$m_i$ is the i-th moment of
% the distribution }of each of the steady state distribution of agents
% for different fraction of fast agents as a function of the
% temperature. and plotted it on figure \ref{fig:binder}
\begin{figure}[h!]
  \centering
  \includegraphics[scale=0.5]{images/varpop/phidiag_varpop.png}
  \caption{Phase diagram of the segregated phase when in the case of
    fixed buy/sell preferences with symmetric settings
    $\pb{1}=1-\pb{2} = 0.2 $ and $\theta_1 = 1-\theta_2 = \theta$ in
    the x range. The red region corresponds to the segregated phase
    when 50\% of the traders are fast and the dashed line is the
    boundary of the segregated phase for an homogeneous population }
  \label{fig:diagvartheta}
\end{figure}

\begin{figure}[h!]
  \centering
  \includegraphics[scale = 0.5]{images/varpop/phi_diag_varpopjpeg.jpg}
  \caption{Segregation temperature of an heterogeneous population of
    traders with fixed buy/sell preferences
    ($p_b^{(1)}= 1- p_b^{(2)}= 0.2$ and
    $\theta_1 = 1- \theta_2 = 0.3$). $\alpha$ is the fraction of slow
    agents in the population }
  \label{fig:phasediag}
\end{figure}

\subsection{Numerical results and discussion}
\label{ssec:disc}

%% INTRO : comment on the numerical results

% Robustness of fp method
% As shown in figure \ref{fig:fpvarpop}, the distribution generated by
% the algorithm is really close\todo{add qualitative datas} to the one
% obtained using numerical simulations. As in the homogeneous case, this
% is an evidence of the robustness of this algorithm.
The two main
features I noticed when fast traders are added to a population
of slow agents are the lowering of the segregation temperature and an
increase of the weight difference between the two peaks of the scores
probability distribution of a segregated population of slow agents.


%% Discussion on the peaks size

In homogeneous population with only slow traders, when segregation
occurs, the two corresponding peaks of the probability distribution
are of a comparable size. When fast traders are added to the population,
segregation is still observed, but the size of the corresponding peaks
is really different as it is shown in figure \ref{fig:fpvarpop}. This
difference between the behaviour of an homogeneous population and an heterogeneous
population comes from the fact that the fast traders have less
information about the markets than the slow ones. The slow traders are
then going to take advantage of that inefficiency to make more profit
(i.e. trade more often at the market where they are profit
oriented). The consequence of this change of behaviour is that the
ratio of the size of the two peaks
$R = N_{\text{profit oriented}}/N_{\text{volume oriented}}$ is going
to increase.
\begin{figure}[h!]
  \centering
  \includegraphics[scale=0.7]{images/varpop/fp05.png}
  \caption{Distribution of the preferences of agents from the first population in the case $\pb{1} = 1 - \pb{2} = 0.2$ and $\theta_1 = 1-\theta_2 = 0.7$ and where half of the population is fast agents. A Kolmogorov Smirnoff test between the distribution of the agents computed using the Fokker Planck equation (the continuous line) and the results of the multi-agent simulation gives a distance of 0.024 between the two distributions. }
  \label{fig:fpvarpop}
\end{figure}
%% Discussion on the segregation temperature

When drawing the phase diagram using the deterministic equations, the
segregation temperature decreases when the fraction of fast agents
increases. This suggests that increasing the fraction of fast agents
inhibits segregation.
% segregation as a limit of the reinforcement learning algorithm
The most obvious reason for this phenomenon is related to the fact
pointed out in the previous paragraph. In the case of an homogeneous
population, there is an exploration exploitation trade-off related to
the reinforcement learning algorithm used for the trading
strategies. It relates to the fact that when you learn actions using
reinforcement learning, you can choose either to favour the
\emph{exploration}: perform sub-optimal actions to improve your
knowledge or to favour the \emph{exploitation}:you do not explore but perform
the best action possible at that time. In the segregated phase, the
small size Gaussian peak corresponds to a population trading by using
a sub-optimal strategy. My intuition is that this sub-optimal choice
is due to the fact that the traders do not explore enough and hence,
have preferences which do not match the real payoff of the two
markets. In section \ref{sec:model} we saw that the temperature was
the parameter used to adjust the exploration exploitation rate. A
segregation temperature corresponds to the temperature at which the
exploration/exploitation rate will become too small to enable the
traders to choose the correct strategy. When adding fast agents to a
population of traders, the score associated to each of the markets are
going to be more and more different. This implies that less
exploitation will be needed to rank the markets efficiently. Hence the
segregation occurs for a lower exploration/exploitation rate. In other
words, the segregation temperature will be lower.
\end{document}
%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "main"
%%% End: 