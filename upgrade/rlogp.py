import numpy as np
var = 0.005
vect = np.arange(-1, 1, 0.01)
for i in vect:
    print i, "\t", (np.exp(-(i-0.5)**2/(2*var)) + np.exp(-(i+0.5)**2/(2*var))), "\t", var*np.log((np.exp(-(i-0.5)**2/(2*var)) + np.exp(-(i+0.5)**2/(2*var))))
