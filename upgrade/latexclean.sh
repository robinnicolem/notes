#!/bin/sh
echo "cleaning all the temporary files in the directory"
rm -f *.out *.blg *.bbl
rm -f *.log 
rm -f *.synctex.gz 
rm -f *.aux 
rm -f *.rel 
rm -rf _region_* auto 
