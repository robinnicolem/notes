Notes on the thesis 
===================

articles
----------
Some notes on the articles I have read

auction_review 
--------------
A sum up of the existing auction mechanism

biblio 
-------
Contain bibliography principal on differential games and dark pools

diffgames 
---------
Sparse notes on differential games

fokker_planck 
--------------
Notes on the fokker Planck equation in the case of an heterogeneous population of agents.

fp_discret 
----------
Notes on the algorithm to solve the fp equation on a discrete lattice

fp\_vs\_mfg
-----------
Comparison between the mean field game results and the Fokker planck equation when preferences are updated using fictitious scores.

mfg_* 
-------
Analytical mean field study in different cases

min_game 
--------
Short sum up on minority games

misc
-----
Results of simulation

nasheq
---------
Review of the different types of Nash equilibria

notations
----------
table of the notations used in the model, this tex file is to be included as an appedix

sumup
-----
draft of the upgrade from MPhil to PhD 
__To be continues__ 
