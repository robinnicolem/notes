#!/bin/sh
for i in $(find -name Makefile)
do
    CWD=$(pwd)
    FPATH=$(echo $i | sed 's/Makefile//') 
    echo $FPATH
    cd $FPATH
    make clean
    cd $CWD 
done
rm $(find -name *.el)
rm $(find -name \#*\#)
rm $(find -name *.upa)
rm $(find -name *.upb)
rm $(find -name *.tmp)
rm $(find -name *.cut)
git rm -r $(find -name auto)
